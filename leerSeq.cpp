#include <iostream>
#include <fstream>

using namespace std;

int main(void)
{ 
   ifstream textFile;
   string path;

   cout << "Ingress path: ";
   getline(cin, path);

   textFile.open(path, ifstream::in);

   // exits program if file cannot be opened 
   if (!textFile.good()) {
      cerr << "File could not be opened" << endl;
   } 
   else { // read from file
      char c;

      c = textFile.get();

      // while not end of file
      while (!textFile.eof()) { 
	      cout << c;
	      c = textFile.get();
      } 

      cout << endl;

      textFile.close(); // closes file   
   } 
} 
