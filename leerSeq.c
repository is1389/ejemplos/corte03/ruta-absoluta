// Reading and printing a sequential file
#include <stdio.h>
#define PATH_LENTH 256

int main(void)
{ 
   FILE *cfPtr; // cfPtr = file pointer
   char path[PATH_LENTH];

   // Get path to file
   printf("%s", "Ingress path: ");
   scanf("%[^\n]", path);

   // fopen opens file; exits program if file cannot be opened 
   if ((cfPtr = fopen(path, "r")) == NULL) {
      puts("File could not be opened");
   } 
   else { // read file by each character until EOF
      char c = fgetc(cfPtr);

      // while not end of file
      while (!feof(cfPtr)) { 
         printf("%c", c);
	 c = fgetc(cfPtr);
      } 

      fclose(cfPtr); // fclose closes the file   
   } 
} 

